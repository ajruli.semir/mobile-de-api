# Mobile De API

## Prerequisites

- MongoDB
- Node 14
## Development

1. Install dependencies

```shell
$ npm i
```

2. Run mongo in docker

```shell
$  docker-compose up -d
```

3. Start service

```
$ npm run dev
```

## Endpoints
### Swagger specification under

`http://localhost:4000/api/swagger.json`


```shell
  GET http://localhost:4000/api/health                   # Health check
  GET http://localhost:4000/api/swagger.json             # API Documentation

  POST http://localhost:4000/api/v1/listings             # Create car listing
  GET http://localhost:4000/api/v1/listings              # Find all car listings
  GET http://localhost:4000/api/v1/listings/{listingId}  # Find car listing by id
```
