FROM node:14-alpine

RUN mkdir -p /app
COPY . /app
WORKDIR /app

RUN npm install --production
RUN rm -f .npmrc

CMD [ "npm", "start" ]
