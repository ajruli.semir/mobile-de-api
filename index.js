const initServer = require('./app/server.js')
const config = require('./app/config.js')

initServer(config)
  .then(server => server.start())
  .catch(error => {
    console.error(error)
    process.nextTick(() => {
      setTimeout(process.exit.bind(null, 1), 5000)
    })
  })
