require('dotenv').config()

const pkg = require('../package.json')

const path = require('path')

const ENV = process.env
const IS_LOCAL = ENV.NODE_ENV === 'local'

module.exports = {
  port: ENV.PORT || 3000,
  isLocal: IS_LOCAL,
  hasDeleteEndpoint: ENV.NODE_ENV !== 'production',
  cors: {
    origin: (ENV.CORS_ORIGINS && ENV.CORS_ORIGINS.split(',')) || []
  },
  logging: {
    level: ENV.LOG_LEVEL || 'debug',
    prettyPrint: IS_LOCAL
  },
  mongo: {
    url: ENV.MONGO_URL,
    db: ENV.MONGO_DATABASE,
    options: {
      appname: 'mobile-de-api',
      ...(ENV.MONGO_USER &&
        ENV.MONGO_PASSWORD && {
          user: ENV.MONGO_USER,
          password: ENV.MONGO_PASSWORD
        }),
      retryWrites: true,
      ssl: ENV.MONGO_SSL === 'true',
      sslValidate: false,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      w: 'majority'
    }
  },
  swagger: {
    info: {
      title: pkg.name,
      version: pkg.version,
      description: pkg.description
    },
    jsonPath: '/api/swagger.json',
    grouping: 'tags',
    reuseDefinitions: false,
    definitionPrefix: 'useLabel',
    deReference: true,
    schemes: IS_LOCAL ? ['http'] : ['https'],
    documentationPage: false,
    swaggerUI: false
  }
}
