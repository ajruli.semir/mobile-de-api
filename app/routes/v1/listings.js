const Validation = require('./helpers/validation/listing-validation.js')
const ListingsHandler = require('../../handler/listings.js')

exports.register = async (server, { permissions }) => {
  server.route({
    method: 'POST',
    path: '/api/v1/listings',
    options: {
      description: 'Create car listing',
      tags: ['api', 'Listings (v1)'],
      validate: {
        payload: Validation.payloadForOneListing,
        options: {
          stripUnknown: true
        }
      },
      response: {
        schema: Validation.responseOfOneListing,
        modify: true,
        options: { stripUnknown: true }
      },
      plugins: {
        'hapi-swagger': {
          responses: Validation.swaggerErrorResponses
        }
      }
    },
    handler: ListingsHandler.createCarListing
  })

  server.route({
    method: 'GET',
    path: '/api/v1/listings/{listingId}',
    options: {
      description: 'Find car listing by id',
      tags: ['api', 'Listings (v1)'],
      validate: {
        params: Validation.paramsForOneListing,
        options: {
          stripUnknown: true
        }
      },
      response: {
        schema: Validation.responseOfOneListing,
        modify: true,
        options: { stripUnknown: true }
      },
      plugins: {
        'hapi-swagger': {
          responses: Validation.swaggerErrorResponses
        }
      }
    },
    handler: ListingsHandler.findCarListingById
  })

  server.route({
    method: 'GET',
    path: '/api/v1/listings',
    options: {
      description: 'Find all car listings',
      tags: ['api', 'Listings (v1)'],
      validate: {
        query: Validation.queryForListings,
        options: {
          stripUnknown: true
        }
      },
      response: {
        schema: Validation.responseOfManyListings,
        modify: true,
        options: { stripUnknown: true }
      },
      plugins: {
        'hapi-swagger': {
          responses: Validation.swaggerErrorResponses
        }
      }
    },
    handler: ListingsHandler.findAllCarListings
  })
}
