const Joi = require('joi')

const { get } = require('lodash')

const uuid = Joi.string()
  .uuid({ version: 'uuidv4' })
  .required()
  .example('6b8c8e1a-42e5-4392-8ca0-451cbc7b2cf3')

const make = Joi.string()
  .valid('BMW', 'Peugeot', 'Fiat', 'Kia', 'Toyota', 'Chevrolet')
  .required()
  .example('BMW')

const model = Joi.string()
  .min(2)
  .max(32)
  .required()
  .example('Series 5')

const buildYear = Joi.number()
  .integer()
  .min(1850)
  .max(new Date().getFullYear())

const description = Joi.string()
  .required()
  .example('This car is **great**. It has the following features: * Low price')

const price = Joi.number()
  .integer()
  .min(0)
  .required()
  .example(16500)

const email = Joi.string()
  .email()
  .required()
  .example('john.doe@example.com')

const imageUrl = Joi.string()
  .uri()
  .example('https://www.netcarshow.com/BMW-5-Series-2021-wallpaper.jpg')

const paramsForOneListing = Joi.object().keys({ listingId: uuid })

const listingFields = Joi.object().keys({
  make,
  model,
  buildYear,
  description,
  price,
  email,
  imageUrl
})

const payloadForOneListing = listingFields
const responseOfOneListing = listingFields.concat(
  Joi.object().keys({ id: uuid })
)

const responseOfManyListings = Joi.array().items(responseOfOneListing)

const queryForListings = Joi.object().keys({
  make: Joi.string().example('BMW'),

  price: Joi.number()
    .integer()
    .example(16500),

  skip: Joi.number().example(25),
  limit: Joi.number()
    .less(100)
    .example(100)
})

module.exports = {
  paramsForOneListing,

  payloadForOneListing,

  responseOfOneListing,
  responseOfManyListings,

  queryForListings,

  swaggerErrorResponses: {
    400: { description: 'Bad Request' },
    401: { description: 'Unauthorized' },
    403: { description: 'Forbidden' },
    404: { description: 'Not Found' },
    500: { description: 'Internal Server Error' },
    503: { description: 'Service Unavailable' }
  }
}
