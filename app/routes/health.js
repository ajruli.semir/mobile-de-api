exports.register = async server => {
  server.route({
    method: 'GET',
    path: '/api/health',
    options: {
      auth: false,
      tags: ['api', 'Monitoring'],
      description: 'Health check',
      plugins: {
        'hapi-swagger': {
          responses: {
            200: { description: 'OK' }
          }
        }
      }
    },
    handler: (req, h) => h.response({ status: 'OK' })
  })
}
