const { get } = require('lodash')

module.exports = {
  name: 'error-logging',
  version: '1.0.0',
  register: async server => {
    server.logger.info('Setup error logging plugin')

    server.ext('onPreResponse', (req, h) => {
      const res = req.response

      if (res.isBoom) {
        server.logger.error(
          {
            params: req.params,
            query: req.query,
            payload: req.payload,
            error: res.output.payload,
            stack: res.isServer ? res.stack : null,
            data: res.data
          },
          res.output.payload.message || 'Unknown error'
        )
      }

      return h.continue
    })
  }
}
