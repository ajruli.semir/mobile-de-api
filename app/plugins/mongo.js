const { ok } = require('assert')
const { MongoClient } = require('mongodb')

class Mongo {
  constructor ({ url, db, options }, server) {
    ok(typeof url === 'string', 'Connection URL must be a string')
    ok(typeof db === 'string', 'DB must be a string')
    ok(typeof options === 'object', 'Options must be an object')
    ok(typeof server === 'object', 'Server must be an object')

    this.server = server
    this.logger = this.server.logger

    this.url = url
    this.db = db
    this.options = options
  }

  async connect () {
    /* istanbul ignore next */
    try {
      this.client = await MongoClient.connect(this.url, this.options)

      this.logger.info(`Connected to mongodb '${this.db}'`)

      this.connection = this.client.db(this.db)

      this.connection
        .on('reconnect', msg => this.logger.warn('Mongo reconnected'))
        .on('timeout', msg => this.logger.warn('Mongo connection timed out'))
        .on('close', msg => this.logger.warn('Mongo connection closed'))
        .on('error', msg => this.logger.warn(`Mongo error: ${msg}`))
    } catch (error) {
      this.logger.error({ error }, 'Could not connect to mongo')
      throw error
    }
  }

  createIndex (collName, keys, options) {
    return this.connection.collection(collName).createIndex(keys, options)
  }

  findAll (collName, query, options = {}) {
    return this.connection
      .collection(collName)
      .find(query, options)
      .toArray()
  }

  findOne (collName, query, options = {}) {
    return this.connection.collection(collName).findOne(query, options)
  }

  async createOne (collName, data, options = {}) {
    const collection = this.connection.collection(collName)
    const {
      ops: [doc]
    } = await collection.insertOne(data, options)
    return doc
  }
}

module.exports = {
  name: 'mongo',
  version: '1.0.0',
  dependencies: {
    'hapi-pino': '*'
  },
  register: async (server, settings) => {
    server.logger.info('Setup MongoDB plugin')

    const db = new Mongo(settings, server)
    await db.connect()

    const models = {
      listing: await require('../models/listing')(db, server)
    }

    server.decorate('request', 'models', models)
  }
}

module.exports.Mongo = Mongo
