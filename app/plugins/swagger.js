module.exports = {
  name: 'swagger',
  version: '1.0.0',
  register: async (server, config = {}) => {
    server.logger.info('Setup swagger plugin')

    await server.register({
      plugin: require('hapi-swagger'),
      options: config
    })
  }
}
