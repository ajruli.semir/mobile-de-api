const { ok } = require('assert')
const { isEqual, pick } = require('lodash')
const { notFound, badRequest } = require('@hapi/boom')
const { v4: UUID } = require('uuid')
const { Mongo } = require('../plugins/mongo.js')
const { ObjectId } = require('mongodb')

const COLLECTION = 'listings'

module.exports = async (db, server) => {
  ok(db instanceof Mongo, 'Argument is not a valid MongoPlugin instance')
  ok(typeof server === 'object', 'Server must be an object')

  try {
    const indices = [
      {
        field: 'id',
        unique: true
      },
      {
        field: 'make',
        unique: false
      },
      {
        field: 'price',
        unique: false
      }
    ]

    const dbOps = indices.map(({ field, unique }) => {
      return db
        .createIndex(COLLECTION, { [field]: 1 }, { unique, background: true })
        .then(() => {
          server.logger.info(
            `Ensured index on field '${field}' on collection '${COLLECTION}'. Unique: ${unique}`
          )
        })
    })

    await Promise.all(dbOps)
  } catch (error) {
    /* istanbul ignore next */
    server.logger.error({ error }, 'Failed to create indices')
  }

  const Model = {
    async findOne (id) {
      const listing = await db.findOne(COLLECTION, { id })
      if (listing) return listing
      throw notFound('Listing not found')
    },

    findAll (query, options) {
      return db.findAll(COLLECTION, query, options)
    },

    async createOne (payload) {
      const data = {
        ...payload,
        id: UUID(),
        _id: ObjectId(),
        createdAt: new Date()
      }

      return db.createOne(COLLECTION, data)
    }
  }

  return Model
}
