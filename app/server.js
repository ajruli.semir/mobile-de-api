const hapi = require('@hapi/hapi')
const Joi = require('joi')
const pkg = require('../package.json')

const initServer = async config => {
  const server = new hapi.Server({
    port: config.port,
    router: {
      isCaseSensitive: true,
      stripTrailingSlash: true
    },
    state: {
      ignoreErrors: true
    },
    routes: {
      cors: {
        ...config.cors,
        headers: ['Accept', 'Authorization', 'Content-Type'],
        additionalHeaders: ['X-Requested-With']
      }
    }
  })

  server.validator(Joi)

  await initPlugins(server, config)
  await initRoutes(server, config)

  server.logger.info(`Launching ${pkg.name} on port ${config.port}`)

  return server
}

const initPlugins = async (server, config) => {
  await server.register({
    plugin: require('hapi-pino'),
    options: config.logging
  })

  await server.register({
    plugin: require('./plugins/mongo.js'),
    options: config.mongo
  })

  await server.register({
    plugin: require('./plugins/swagger.js'),
    options: config.swagger
  })

  await server.register({
    plugin: require('./plugins/error-logging.js')
  })
}

const initRoutes = async (server, config) => {
  await require('./routes/health').register(server)
  await require('./routes/v1/').register(server, config)
}

module.exports = initServer
