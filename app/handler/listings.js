module.exports = {
  findCarListingById (req) {
    const {
      server,
      models,
      params: { listingId }
    } = req

    server.logger.debug(`Find car listing w/ ${listingId}`)
    return models.listing.findOne(listingId)
  },

  findAllCarListings (req) {
    const { server, models, query } = req

    const { skip = 0, limit = 50, make, price } = query

    const options = {
      sort: { createdAt: -1 },
      limit,
      skip
    }

    const makeQuery = make ? { make } : {}
    const priceQuery = price ? { price } : {}

    const listingQuery = {
      ...makeQuery,
      ...priceQuery
    }

    server.logger.debug('Find car listings')
    return models.listing.findAll(listingQuery, options)
  },

  createCarListing (req, h) {
    const { server, models, payload } = req

    server.logger.debug('Create car listing')
    return models.listing.createOne(payload)
  }
}
