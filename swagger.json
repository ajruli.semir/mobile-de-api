{
  "swagger": "2.0",
  "host": "localhost:4000",
  "basePath": "/",
  "schemes": [
    "http"
  ],
  "info": {
    "title": "mobile-de-api",
    "version": "1.0.0",
    "description": "The miniature version of mobile.de"
  },
  "tags": [],
  "paths": {
    "/api/health": {
      "get": {
        "summary": "Health check",
        "operationId": "getApiHealth",
        "tags": [
          "Monitoring"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "type": "string"
            }
          }
        }
      }
    },
    "/api/v1/listings": {
      "get": {
        "summary": "Find all car listings",
        "operationId": "getApiV1Listings",
        "parameters": [
          {
            "type": "string",
            "name": "make",
            "in": "query"
          },
          {
            "type": "integer",
            "name": "price",
            "in": "query"
          },
          {
            "type": "number",
            "name": "skip",
            "in": "query"
          },
          {
            "type": "number",
            "x-constraint": {
              "less": 100
            },
            "name": "limit",
            "in": "query"
          }
        ],
        "tags": [
          "Listings (v1)"
        ],
        "responses": {
          "200": {
            "schema": {
              "type": "array",
              "items": {
                "type": "object",
                "properties": {
                  "make": {
                    "type": "string",
                    "example": "BMW",
                    "enum": [
                      "BMW",
                      "Peugeot",
                      "Fiat",
                      "Kia",
                      "Toyota",
                      "Chevrolet"
                    ]
                  },
                  "model": {
                    "type": "string",
                    "example": "Series 5",
                    "minLength": 2,
                    "maxLength": 32
                  },
                  "buildYear": {
                    "type": "integer",
                    "minimum": 1850,
                    "maximum": 2021
                  },
                  "description": {
                    "type": "string",
                    "example": "This car is **great**. It has the following features: * Low price"
                  },
                  "price": {
                    "type": "integer",
                    "example": 16500,
                    "minimum": 0
                  },
                  "email": {
                    "type": "string",
                    "example": "john.doe@example.com",
                    "x-format": {
                      "email": true
                    }
                  },
                  "imageUrl": {
                    "type": "string",
                    "example": "https://www.netcarshow.com/BMW-5-Series-2021-wallpaper.jpg",
                    "x-format": {
                      "uri": true
                    }
                  },
                  "id": {
                    "type": "string",
                    "example": "6b8c8e1a-42e5-4392-8ca0-451cbc7b2cf3",
                    "x-format": {
                      "guid": {
                        "version": "uuidv4"
                      }
                    }
                  }
                },
                "required": [
                  "make",
                  "model",
                  "description",
                  "price",
                  "email",
                  "id"
                ]
              }
            },
            "description": "Successful"
          },
          "400": {
            "description": "Bad Request"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          },
          "500": {
            "description": "Internal Server Error"
          },
          "503": {
            "description": "Service Unavailable"
          }
        }
      },
      "post": {
        "summary": "Create car listing",
        "operationId": "postApiV1Listings",
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "schema": {
              "type": "object",
              "properties": {
                "make": {
                  "type": "string",
                  "example": "BMW",
                  "enum": [
                    "BMW",
                    "Peugeot",
                    "Fiat",
                    "Kia",
                    "Toyota",
                    "Chevrolet"
                  ]
                },
                "model": {
                  "type": "string",
                  "example": "Series 5",
                  "minLength": 2,
                  "maxLength": 32
                },
                "buildYear": {
                  "type": "integer",
                  "minimum": 1850,
                  "maximum": 2021
                },
                "description": {
                  "type": "string",
                  "example": "This car is **great**. It has the following features: * Low price"
                },
                "price": {
                  "type": "integer",
                  "example": 16500,
                  "minimum": 0
                },
                "email": {
                  "type": "string",
                  "example": "john.doe@example.com",
                  "x-format": {
                    "email": true
                  }
                },
                "imageUrl": {
                  "type": "string",
                  "example": "https://www.netcarshow.com/BMW-5-Series-2021-wallpaper.jpg",
                  "x-format": {
                    "uri": true
                  }
                }
              },
              "required": [
                "make",
                "model",
                "description",
                "price",
                "email"
              ]
            }
          }
        ],
        "tags": [
          "Listings (v1)"
        ],
        "responses": {
          "200": {
            "schema": {
              "type": "object",
              "properties": {
                "make": {
                  "type": "string",
                  "example": "BMW",
                  "enum": [
                    "BMW",
                    "Peugeot",
                    "Fiat",
                    "Kia",
                    "Toyota",
                    "Chevrolet"
                  ]
                },
                "model": {
                  "type": "string",
                  "example": "Series 5",
                  "minLength": 2,
                  "maxLength": 32
                },
                "buildYear": {
                  "type": "integer",
                  "minimum": 1850,
                  "maximum": 2021
                },
                "description": {
                  "type": "string",
                  "example": "This car is **great**. It has the following features: * Low price"
                },
                "price": {
                  "type": "integer",
                  "example": 16500,
                  "minimum": 0
                },
                "email": {
                  "type": "string",
                  "example": "john.doe@example.com",
                  "x-format": {
                    "email": true
                  }
                },
                "imageUrl": {
                  "type": "string",
                  "example": "https://www.netcarshow.com/BMW-5-Series-2021-wallpaper.jpg",
                  "x-format": {
                    "uri": true
                  }
                },
                "id": {
                  "type": "string",
                  "example": "6b8c8e1a-42e5-4392-8ca0-451cbc7b2cf3",
                  "x-format": {
                    "guid": {
                      "version": "uuidv4"
                    }
                  }
                }
              },
              "required": [
                "make",
                "model",
                "description",
                "price",
                "email",
                "id"
              ]
            },
            "description": "Successful"
          },
          "400": {
            "description": "Bad Request"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          },
          "500": {
            "description": "Internal Server Error"
          },
          "503": {
            "description": "Service Unavailable"
          }
        }
      }
    },
    "/api/v1/listings/{listingId}": {
      "get": {
        "summary": "Find car listing by id",
        "operationId": "getApiV1ListingsListingid",
        "parameters": [
          {
            "type": "string",
            "x-format": {
              "guid": {
                "version": "uuidv4"
              }
            },
            "name": "listingId",
            "in": "path",
            "required": true
          }
        ],
        "tags": [
          "Listings (v1)"
        ],
        "responses": {
          "200": {
            "schema": {
              "type": "object",
              "properties": {
                "make": {
                  "type": "string",
                  "example": "BMW",
                  "enum": [
                    "BMW",
                    "Peugeot",
                    "Fiat",
                    "Kia",
                    "Toyota",
                    "Chevrolet"
                  ]
                },
                "model": {
                  "type": "string",
                  "example": "Series 5",
                  "minLength": 2,
                  "maxLength": 32
                },
                "buildYear": {
                  "type": "integer",
                  "minimum": 1850,
                  "maximum": 2021
                },
                "description": {
                  "type": "string",
                  "example": "This car is **great**. It has the following features: * Low price"
                },
                "price": {
                  "type": "integer",
                  "example": 16500,
                  "minimum": 0
                },
                "email": {
                  "type": "string",
                  "example": "john.doe@example.com",
                  "x-format": {
                    "email": true
                  }
                },
                "imageUrl": {
                  "type": "string",
                  "example": "https://www.netcarshow.com/BMW-5-Series-2021-wallpaper.jpg",
                  "x-format": {
                    "uri": true
                  }
                },
                "id": {
                  "type": "string",
                  "example": "6b8c8e1a-42e5-4392-8ca0-451cbc7b2cf3",
                  "x-format": {
                    "guid": {
                      "version": "uuidv4"
                    }
                  }
                }
              },
              "required": [
                "make",
                "model",
                "description",
                "price",
                "email",
                "id"
              ]
            },
            "description": "Successful"
          },
          "400": {
            "description": "Bad Request"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          },
          "500": {
            "description": "Internal Server Error"
          },
          "503": {
            "description": "Service Unavailable"
          }
        }
      }
    }
  }
}
